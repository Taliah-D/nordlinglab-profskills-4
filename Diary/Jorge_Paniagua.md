This diary file is written by Jorge Paniagua E24097023 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-23 #

* I am learning step by step how to use this new tool I dind't use before called Bitbucket.
* From the first group presentation I realized that almost anything in the world use batteries and this devices may provably become the main energy souces for everything in the world.
* For the future, it seems to be unlogical to invest in metro systems anymore, since using autonomous vehicles is getting cheaper and more convenient.
* I learned about the international standard for dates and time - ISO 8601.


# 2021-09-30 #

* Most of the time to have a better understanding of some statistic results you need to understand the relationship between the one who did it and the analized data.
* You need to ask yourself if you're related to the data.
* Take more in count public statistics over private statistics.
* Fake news may be more dangerous than real weapons.


# 2021-10-07 #

* Consider the possibility that history could be changed and manipulated through fake news.
* A certain part of Taiwan's growth is apparently thanks to loans.
* The world economy has a great dependence on the dollar.


# 2021-10-14 #

* If a company knows you much more than yourself, they can control and manipulate your own deepest emotions and desires without even realizing it.
* You should never underestimate human stupidity.  It is one of the most powerful forces shaping history.
* NTD purchasing power today is much lower than its purchasing power 30 years ago.


# 2021-10-21 #

* Exercise is good for improving memory.
* You cannot assume that a claim is true just because it cannot be proven that it is false yet and vice versa.
* I am impressed by the percentage of people who are worried, stressed and anxious about climate change.
* I think I already got used to using GIT repository, it already catched my attention.
* I may make some researchs in the future about more fuctions of Bitbucket in order to use the most of it. 

# 2021-10-28 #
* Depression is a very complex problem to take it easy, the best way to deal with it is by looking for professionals help.
* Some people may think that talking to people with depression is easy, but the truth is that any wrong word or sentence could trigger results that make the situation turn even worse.
* When someone(with possible depression) wants to share their emotions with you, just try to listen and understand them if you are not capable enough to continue the conversation.
* After this lecture I took some time researching about this topic and reading some articles and experiences of some people who went through situations related to it, I think it is necessary for people wants to help their friends but don't know how to help them.
* It is interesting to analyze the fact that a lot of chemical reactions can cause certain kinds of feelings in living beings. This topic led me to question myself about how we are affected by things we don't even knoe about. Also, assuming the fact that we are made up of atoms, which are supposed to be lifeless, but the set of a few atoms creates cells, molecules, organs, and human beings, which have life. How is it that a set of lifeless things can create life?


# 2021-11-04 #

* I learned about this new tool called MangaX
* I guess it might be hard to decide whether to stay or quit when you are in a good paid company with a bad work environment.
* "If you pit a good performer against a bad process, the process will win almost every time" I kinda liked this statement.


# 2021-11-11 #

* It is interesting to lean the definition of Legal Dictionary, although a bit annoying because it aroused my curiosity and I would like to have a good knowledge and handling of the words it contains since I feel that at some point in my life it might be useful for me as a basic tool, but since I don't have that much time available lately to memorize things, in the next few days I might probably just go throught my life normally ignoring its existence.
* I do not know how necessary some words are within the Legal dictionary, according to my knowledge some of the words that were used as an example have a very similar meaning to the ones we use in everyday language, what's more, nowadays there are dictionaries that classify words depending on the area in which they are to be used, so many words in the Legal dictionary are probably already defined in some other dictionary without necessarily being a Legal dictionary.
* At the end of the reading, I do not feel that one of the most useful features of a Legal dictionary is in redefining the meaning of certain words from everyday language to be used in the legal system.
* To some point, it seems to me that their meanings are still very close, and perhaps the same. 
* From my point of view, the only good purpose of having a Legal dictionary is more for convenience, since all the most frequently used words in the legal system are collected and compressed in the same place, which makes finding those words and accessing their meanings faster, much more efficient than looking for them in a common dictionary where there are much more combinations.
* Despite all of what I just wrote, I consider that the legal system is quite complex and it is very likely that it is more difficult for someone that has no any major related to the area to understand deeply the real importance of a Legal dictionary. Perhaps the difference is in certain expressions or terminologies to redefine a word, expressions that seem insignificant and imperceptible to someone like me, but that being in court has a great difference and greatly influences the result.
* It was a bit tiring to write this much, I should focus more on my second midterms but I'm tired after writing this diary xd. 


# 2021-11-18 #
Lecture.10 (No lecture due to professor need to give a speech in an activity)



# 2021-11-25 #

* I think that assuming the fact that each human being is alone on some planet separated from the rest would be much less problematic and more comfortable for each one, because it would not be necessary to apply unnecessary rules for a good coexistence.  Being alone somewhere would give one the opportunity to do what one wants without depriving another person of his personal rights.  But when we are living with other individuals, it is necessary to have rules taking into account the common good and that the sum of each individual benefit is the maximum.
* As a continuation of what has already been said before. Since we are coexisting with other individuals, I believe that these rules are more than necessary to have a reference line that no person should cross in order not to affect the rights of the another.
* When living with other individuals leads us to think about the need to reach a consensus to build certain rules, the only problem is that it is quite difficult to reach a consensus with individuals who in many cases have different ways of thinking and therefore their interests are different from the ones we might have.


# 2021-12-02 #

* I'm much more tired than I was the last week xd.
* Midterms hits hard.
* I had an USA VISA interview last week so I couldn't attend to class.
* Luckily the interview went well, it did not last more than 3 minutes. Next to me was a man who was denied the VISA and he was complaining for a few minutes, didn't want to accept the fact that he did not get the VISA.
* Before making the 請假, I made sure to make the most part of our group project so that my group members don't feel angry because of me not going to class.
* I hope my group made an excellent presentation.
* Well, according to my experience with them, they are people who know how to improvise, so I don't think they had any big problem at the time of the group's presentation.
* I believe in them.
* I feel that by not going to only one lecture, I am very lost in terms of the content given and I have no idea how exactly the next task should be done.
* I wish that by just reading the professor's pptx I alreaDY can understand clearly all the lecture, although on second thought I do not think that any professor would prepare a course this efficient, otherwise the professor would be left without students who attend their classes and therefore the professor would have no job, which I don't think is on any proffesor's wish list.
* After reading the pptx of the last lecture, I at least realized that they were talking about "How to make sense of events" which is more about perspectives.
* They saw three videos.
* With the advent of the Internet and social media, news is distributed at an incredible rate by an unprecedented number of different media outlets. How do we choose which news to consume? Damon Brown gives the inside scoop on how the opinions and facts (and sometimes non-facts) make their way into the news and how the smart reader can tell them apart.
* From about 1500 BC to 1200 BC, the Mediterranean region played host to a complex cosmopolitan and globalized world-system. It may have been this very internationalism that contributed to the apocalyptic disaster that ended the Bronze Age. When the end came, the civilized and international world of the Mediterranean regions came to a dramatic halt in a vast area stretching from Greece and Italy in the west to Egypt, Canaan, and Mesopotamia in the east. Large empires and small kingdoms collapsed rapidly. With their end came the world’s first recorded Dark Ages. It was not until centuries later that a new cultural renaissance emerged in Greece and the other affected areas, setting the stage for the evolution of Western society as we know it today. Professor Eric H. Cline of The George Washington University will explore why the Bronze Age came to an end and whether the collapse of those ancient civilizations might hold some warnings for our current society.
* An example of how historical comparison helps to make sense of events. "Mike Maloney draws eerie parallels to the misguided leaders and monetary policies that doomed civilizations from Ancient Rome to modern-day America.


# 2021-12-09 #

* Today's lecture was a bit difficult to understand for me. 
* I don't feel quite familiar with the topic.
* I hope I'll have a better understanding of the next lecture.


# 2021-12-16 #

* Today's class was quite interesting.
* We had big group presentation.
* Although I was tired enough to be able to concentrate on the content of the presentations, I could enjoy a bit of the lecture.
* Since this time I did not have much time to help my group members, I thought that for this time presentation we were not going to finish the work on time, but somehow I was surprised by the desire that the members of my group put in to finish the presentation.
* We finished the job a few minutes early and were able to present it successfully.
* Anyway, there were several things to improve in the presentation, the professor gave us some new and more specific indicators (questions) to follow, so I guess the next presentation may be much better.
* I found it a bit funny that the best way to avoid excessive and unwanted CO2 generation is to stop having children.  In fact, I quite like the idea, in addition to reducing CO2 production, I think it also helps a lot to reduce unnecessary expenses that one could be having.  Caring for a child until at least 18 years old is quite a risky and expensive investment.
* Midterms are still hitting hard ;(


# 2021-12-23 #

* A. Successful and productive.
* B. I was preparing for my microelectronics test all the night before the test and when I took it I felt quite confident about my knowledge when answering all the questions (I actually surpassed the scope I was supposed to study for the test, this is because I almost never go to class and just study by myself, so I sometimes don't know the real scope xD. I guess sometimes not going to class has its advantages). Then, even being so tired after the exam, I still could attend to today's professional skills lecture and have some fun with my classmates.
* C. I'll get up early tomorrow and go to excersice a bit before starting to study for my finals.